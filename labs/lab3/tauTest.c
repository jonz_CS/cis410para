#include<time.h>
#include<stdio.h>
#include<stdlib.h>
#define ARRAYSIZE 20
#define THREADS 4



int main()
{
   int i=0;
   omp_set_num_threads(THREADS);
   int* values=(int*)malloc(sizeof(int)*omp_get_num_threads());
   int threadNum = 0;
   int * vector1 = (int*)malloc(ARRAYSIZE * sizeof(int));
   int * vector2 = (int*)malloc(ARRAYSIZE * sizeof(int));
   int * vector3 = (int*)malloc(ARRAYSIZE * sizeof(int));
   //do this in parallel. Give each thread a threadNum, but they all get the same array
   #pragma omp parallel private(threadNum) shared(vector1)
   {
      //give me your threadNum
      threadNum=omp_get_thread_num();
      int j;
      for (j = threadNum; j < ARRAYSIZE; j += omp_get_num_threads()) {
	vector3[j] = vector2[j]+vector1[j];
      }
   }
   //prove that you actually did something
   for(i=0;i<THREADS;i++)
   {
       printf("Thread %d calculated %d\n",i,values[i]);
   }
}
