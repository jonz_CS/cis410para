From reading the profiles I gained the following insights:

OpenMP is really weird. It may or may not cause threads to block following
their completion, rather than just making them block like a normal parallel
library.

Evenly distributing a load results in reduced idle time between CPUs. If the
way that load is distributed between processors is not proportional to their
ability, then one processor may finish before another, resulting in wasted
computational power. (Noting that "proportional to their ability" does not
mean "equal" in all cases; specifically when processors that do not have the
same abilities are used in the same computer or computer system, like GPUs.)

