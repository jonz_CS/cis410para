This should be replaced by a description of the data reorganization you chose to implement and its effectiveness. Your description can be short, but should address the change made and why you expected a performance improvement. You should also include performance metrics to support wether runtime improved or not.

My data reorganization was an extremely simple one. It consisted of the following:
-Transpose array B internally. (This was done by swapping its dimensions in its generation, since they are all identical.)
-Instead of making array accesses to A and B directly the row of A and the column of B that are being multiplied at the time are given pointers to them, and are multiplied together like vectors. (That is, the dot product.)
I expected a performance increase from this because of all of the following:
-Half as much array dereferencing of elements in A and B occurs.
-Much more importantly, this way, the elements of B are in the order they're accessed in memory. This way, only the row of A and the column of B that are currently being multiplied need to be stored in the cache at any given time. (This was already true of the row of A, but the compiler may not have known that that was true.) This is a result of transposition.

The parallelization was on the outermost loop of the matrix multiplication, i.e., the algorithm fills across C in parallel in one dimension and in serial in the other. This still allows for all four processors to effectively be employed simultaneously, hopefully in tandem with using less cache as a result of my changes.
Oddly, my attempts to put parallelism at a deeper level did not function as expected. One such attempt is stored in the repository under the name "treematmul.c". It attempts to multiply and reduce elements within the row and column of the matrix in parallel whereas they are normally summed in serial, and should run at roughly O(n^3 log n) and make full use of parallelization. It does not run as well as the serial or naive parallel implementations on any data size I tested it at.

My conclusion is that it's magic or something.
