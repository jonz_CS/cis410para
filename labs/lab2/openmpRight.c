#include<time.h>
#define ARRAYSIZE 100
#define STEP_SIZE 4 //Must be a factor of ARRAYSIZE...yeah
void init(int* a, int size)
{
  int i=0;
  #pragma omp parallel for
  for(i=0;i<size;i++)
  {
    a[i]=i;
  }
}
void scanOver(int* a,int st,int len) {
	int i;
	for (i=0;i<len;i++) {
		if (i>0) a[st+i] = a[st+i]+a[st+i-1];
	}
}
int main()
{
   srand(time(NULL));
   int i=0;
   int* a=(int*)malloc(sizeof(int)*ARRAYSIZE);
   init(a,ARRAYSIZE);
   omp_set_num_threads(4);
   #pragma omp parallel for
   for(i=0;i<ARRAYSIZE;i+=STEP_SIZE)
   {
        scanOver(a,i,STEP_SIZE);
   }
   printf("This part worked\n");
   for (i=0;i<ARRAYSIZE;i+=STEP_SIZE) {
	if (i > 0) {
	   int j;
           for (j=0;j<STEP_SIZE;j++) {
               a[i+j] =a[i+j]+a[i-1];
           }
	}
   }



 
   
   for(i=0;i<ARRAYSIZE;i++)
   {
      printf("Index: %d, Value: %d\n",i,a[i]);
   }
}
