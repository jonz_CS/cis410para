Jon Zeller
5/6/2014
CIS 410 Parallel
HW 3


Data Reorganization

1. Cacheing. Putting the data so that you have to less frequently make accesses to slower forms of storage such as memory or especially page files. This makes the algorithms run faster.

2. a. Because it's essentially a continuous array of size 1,000,000,000, it's likely impossible for the processors to load the entire array into their caches at once.
   b. Because the processors must frequently read and write to memory, performance will more than likely decrease, compared to if they were able to keep working memory in cache.
   c. Personally? I'd change the looping, so that each processor would pay attention to either 1000 or 1,000,000 data locations at once, so that it could store them in its cache. In C/C++ I would do this by dereferencing the array, but not necessarily here.
   d. Geometric decomposition. It depends on the shape of the stencil, but break the array into e.g. a 100x100x100 array of 10x10x10 cubes, which aren't actually 10x10x10 but have ghost entries from adjacent cubes as well so that they can be used in a stencil. This is not quite the same as my suggestion above, but it's workable.


Stencil Pattern

1. A 3x3x3 cube is actually 27 cells.
   a. The ghost cells will be a buffer of 1 cell in each direction around each 500x500x500 cube. Each buffer zone is of total size 6(500*500) (faces)+12(500) (edges) +8 (corners) = 1506008 cells, for a total buffer size of 12048064 ghost cells compared to 1000000000 written cells.
   b. 1000=250*4 so there would be 4 chunks counted on an edge; by extention, there would be 4x4=16 chunks making up any face of the cube, and 4x4x4=64 chunks in all. 1000 = 125*8, so 64 chunks make a face and 512 chunks make the whole cube by the same calculations.
   c. Where e = the edge length of the chunk, g = the number of ghost cells, c is the true number of cells per chunk, nc is the number of chunks, gc is the number of ghost cells to per chunk, and r is the ratio of ghost cells to true cells:
c = e*e*e.
gc = 6(e*e)+12e+8 = (e+2)(e+2)(e+2)-(e*e*e).
nc = (1000/e)^3.
g = gc*nc.
r = g/(1,000,000,000).
r(250)=(6(250)(250)+12(250)+8)*64/1,000,000,000 = 0.024192512
r(125)=(6(125)(125)+12(125)+8)*512/1,000,000,000 = 0.048772096
As the ratio increases, the amount of memory used on ghost cells increases. Furthermore, if we are doing an iterated stencil on the same blocks, we must synchronize more stuff more frequently. 

2. Yes. If we increase the depth then we can do synchronizes less often: for every increase of 1 in the halo's depth, we can run one additional iteration without synchronization. If we make it too large, especially with smaller chunk sizes, then it will take more and more memory; at some point it will outstrip the amount of memory used on the actual data, missing the point. Where h is the halo depth and e is the chunk edge length, the number of ghost cells per chunk = (e+2h)^3 -e^3, where ^ is the exponentiation operator, not the AND operator, as in C/C++.

3. Instead of looping over the array by raw index, it's approximately like...
inline void shorthand(i,j) { A[i,j]=f(A[i-1,j]+A[i,j-1]+A[i-1,j-1]+B[i,j])};
for (i=0;i<max(m,n);i++) {
   parallel_for (j=i;j>=0;j--) {
	//Branch out along the diagonal
	//Also there needs to be array bounds checking here
	//But they aren't in the given stuff either so I'll ignore it
	shorthand(i-j,i+j);
	shorthand(i+j,i-j);
   }
}
I.e., going diagonal line by diagonal line. Each top-right-to-bottom-left (TRTBL) diagonal line is dependent only on elements on the previous two TRTBL lines and on elements in the same TRTBL line in B; elements in it are not interdependent, so they can be done in parallel.

