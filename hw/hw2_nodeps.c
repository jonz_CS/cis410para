#include <stdlib.h>
#include <stdio.h>

//Demonstrates that no dependencies exist in problem 1.
int main() {

	//Initialization.
	int stuff[20][20];
	int i;
	int j;
	//Initialize everything to 1. 1 memory access was involved in each cell.
	for (i=0;i<20;i++) for (j=0;j<20;j++) {
		stuff[i][j]=1;
	}
	
	//Actual loop. It only goes over 20, but that's more than enough.
	for (i=4;i<20;i++) {
		for (j=i-2;j<=i;j++) {
			//Add the things, so we can see dependencies.
			//If we read from something we wrote in the loop it adds 2 to the target instead of 1.
			stuff[i][j] += stuff[i-4][j];
		}
	}
	
	//Print. If we had any dependencies there should be 3s or higher.
	for (i=0;i<20;i++) {
		for (j=0;j<20;j++) {
			printf ("%d ",stuff[j][i]);
		}
		printf("\n");
	}
}